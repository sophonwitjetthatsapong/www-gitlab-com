---
layout: markdown_page
title: "Category Vision - Secret Detection"
---

- TOC
{:toc}

## Description

### Overview

Secret Detection aims to prevent that sensitive information, like passwords, authentication tokens, and private keys are unintentionally leaked as part of the repository content.

It checks files and configuration to detect well-known variable names and files, and reports if there is something that is potentially risky to share.

It doesn't target a specific language, but the ruleset can be applied to any project.

### Goal

Our goal is to provide Secret Detection as part of the standard development process. This means that Secret Detection is executed every time a new commit is pushed to a branch. We also want to include Secret Detection as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Secret Detection results can be consumed in the merge request, where only new vulnerabilities, introduced by the new code, are shown.

There is also a high request to block a remote push if it contains secrets. This is not a simple problem to address, because it implies solving the following problems:
1. the detection should complete in a reasonable time (it is a synchronous operation)
1. false positives may block legitimate commits with no way to bypass that

## What's Next & Why

We want to leverage existing open source tools to provide a first MVC for Secret Detection.

The next MVC is to perform secret detection on full history of the repository: https://gitlab.com/gitlab-org/gitlab-ee/issues/9508

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/675)
 
## Competitive Landscape

GitHub offers [Token Scanning](https://help.github.com/articles/about-token-scanning/) that aims to detect secrets in public repositories. The approach is quite different, as the scan is performed asynchronously and users are not notified, but service providers are.

## Analyst Landscape

Secret detection is not actually part of any Analyst report. Anyway, since we consider it related to SAST, we can raise awareness in that specific context.

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=analysts&label_name[]=secret%20detection)

## Top Customer Success/Sales Issue(s)

- Create a SAST analyzer to detect secrets in repositories: https://gitlab.com/gitlab-org/gitlab-ee/issues/6719
- Forbid remote push if commit contains secrets: https://gitlab.com/gitlab-org/gitlab-ee/issues/8792

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=customer&label_name[]=secret%20detection)

## Top user issue(s)

- Create a SAST analyzer to detect secrets in repositories: https://gitlab.com/gitlab-org/gitlab-ee/issues/6719

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=popularity&label_name[]=secret%20detection)

## Top internal customer issue(s)

Currently we don't have any request from our internal customers.

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&sort=milestone&label_name[]=internal%20customer&label_name[]=secret%20detection)

## Top Vision Item(s)

- Create a SAST analyzer to detect secrets in repositories: https://gitlab.com/gitlab-org/gitlab-ee/issues/6719