---
layout: markdown_page
title: "Uploading Executed Contracts to ContractWorks"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Steps to Filing Your New Vendor Contract in ContractWorks

This process is used to file your contract or related vendor documents after they are fully executed (signed by all parties).  For the full contract approval process, see the [Vendor Contracts and Invoice Payment](/handbook/finance/procure-to-pay/) page.

### Requesting a ContractWorks Log-in

If you have not used ContractWorks yet, request a log-in with one of the following methods:
1. Mention @gl-legal-team in an existing contract-related issue.
2. Send an e-mail to `legal@` to create  a new issue in the legal issue tracker.

You will receive an e-mail invitation to log in and file your fully executed contract.


### Uploading the Document

Please watch the [ContractWorks tutorial](https://securedocs.wistia.com/medias/odpfq6gnit) for uploading, filing, and downloading documents.

#### Step 1: Open the Correct File for Your Upload

Use the folder navigation panel on the left side of your screen to open your department’s file. Some departments have subfolders for categories or vendors. If your contract would best fit in one of the subfolders, go ahead and open it up as well.

#### Step 2: Upload Your Fully Executed Contract

Once you have the correct file selected, uploading is very simple. You can either drag and drop into the files panel on the right or use the blue `upload` button to browse your files. Please ensure that the contract version you upload is the signed pdf.

![upload](./upload.png)

#### Step 3: Standardize Your File Name

Click on the pencil icon at the right-most edge of your file information line to edit the file name and name according to the example below:

Vendor Name-GitLab Contract Type - yyyy.mm.dd.pdf

There should not be any underscores in your file name. Spaces between words are fine. Contract Type will normally be the heading you find on the contract itself. Some examples include “Master Service Agreement”, “Partnership Incentive Agreement”, “Addendum to Subscription Terms”. The date should be the Effective Date, which is usually the date when the last signature is added (unless it is specified otherwise in the contract).


### Tagging the Document

Please watch the [ContractWorks tutorial](https://securedocs.wistia.com/medias/28tju1vqmw) for tagging your document.



#### Step 1: Choose the Correct Template for Your Contract Type.

Click on the greyed out tags in your file line. This will take you to a screen entitled “Manage Tags”. From the drop-down menu, select the template that best fits your contract.

![tag](./tag.png)

![template](./template.png)

#### Step 2: Tag Your Contract.

Fill in the tags with the information you collected during the contract approval process. See details below for best practices for recurrence, notifications, and attaching documents to a Master Agreement.

When you’ve filled everything in, click Submit.

#### Best Practices for Tagging

##### Auto-renewal Recurrences

Recurrences will program ContractWorks to change the start and/or end dates for your contract as it auto-renews. Simply select the option and set the number of months of your contract term. ContractWorks will then auto-change the dates when the appropriate time comes.

##### Notifications

You can watch the [training video](https://securedocs.wistia.com/medias/28tju1vqmw) from ContractWorks and use the below standards to set notifications for your contracts. (The part of the video relevant to Notifications begins at 00:55)

To set notifications for start and end dates, select the `Enable Notifications` option. This will bring up 3 different fields to fill in. Follow the best practices below for each type of notification.

###### Start Date

1. Set the days to 180
2. Input your e-mail address  
3. Use the following message: This contract will auto-renew in 6 months. If this vendor is meeting the needs you had for it, you do not need to do anything at this time. If you are not happy with how this vendor is working out, please begin researching other options.

###### End Date

1. Set the days 30 day before termination notice is due. (For example, if we need to give 30 days’ notice, set the notification to 60 days.)
2. Input your e-mail address and [legal@gitlab.com](mailto:legal@gitlab.com)
3. Use the following message: This contract will auto-renew in xx days. If you do not wish to continue with this vendor, you must give x days’ notice.

##### Legal Terms

Legal Terms need to be filled in by the owner of the contract upon uploading. The following explanations will help you understand what information to include. If you have specific questions about your contract, please contact someone in legal for input.

**Notice Contact and Address(es)** Some companies will have special addresses for legal notices to be sent. Search the document for "notices" and make note of any specific addresses or people who need to have notices.<br/>
**Method of Notice** Notices can be delivered in person, by mail, by e-mail, by fax, and so forth. If a company specifies which methods they will accept, designate all that apply on this line.<br/>
**GitLab legal entity** This is a drop-down menu of the different options for legal entities. This should be specified in the top paragraph of your contract where the parties are listed.<br/>
**Start Date** If there is an Effective Date in the first paragraph of your contract, input this date as the start date. If there is no Effective Date defined, use the date of the last signature.<br/>
**Term (# of years)** You can search for "Term" until you get to the section that details the length of the contract. Some will not have a term because they are ongoing until someone terminates.<br/>
**Termination Notes** Capture the time frame for notice that is required to terminate. Look for how many days' notice is required before terminating for a breach of contract and how many days' notice is required for terminating for convenience or any reason.<br/>
**Auto Renew?** If your contract has a term, make sure you note whether the contract will auto-renew or not. This information is usually in the same section as the term length.<br/>
**End Date** Most contracts do not specify an end date, even if it has a term defined. Use the information you have for the term to figure out the end date. The end date will be one day before the anniversary date of the start date. For example, a 12-month contract starting on Sept 27, 2018 will end on Sept 26, 2019.<br/>
**Payment Terms** This is the amount of time Accounts Payable has to pay the bill and can usually be found in a section called "Fees and Payment" or something similar. It is typically 15, 30, or 45 days. Choose the correct number from the drop-down. If the payment term is not listed, choose other and then document the number on the "Payment Term Notes" line.<br/>
**Payment Terms Notes** Make note of any late fees here.<br/>
**Indemnities** Search the document for "indem" and copy and paste the section into this field.<br/>
**Contract Type** Choose from the drop-down the category that best fits the purpose for entering this contract.<br/>
**Document Type** Use the heading of your contract to determine the best fit from the drop-down menu.<br/>

### Attaching the Document

If your document is an Order Form, Addendum, or other sub-document that is connected to a Master Agreement, you will need to attach the document to the Master Agreement, as detailed below:

1. Go to the folder where your Order Form and Master Agreement are filed. (Ensure that your Order Form and Master agreement are filed in the same folder, as attaching may not work across different folders.)
2. Select the new document by clicking the checkbox on the left end of the contract information line. This will make the options along the top of the file list blue and clickable.
3. Click `Attach`
4. Hover over the Master Agreement and click the paperclip that has appeared next to the edit pencil.

### Linking to the Document

There may be times when you need to send your executed contract to another GitLab team-member, such as when invoices need to be paid.

1. Navigate to your uploaded contract.
2. Click on the contract file name, which will open a watermarked version of the contract in a new tab.
3. Use the new tab's unique URL to link the document.

