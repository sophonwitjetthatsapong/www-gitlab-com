---
layout: markdown_page
title: "SDM.1.01 - System Documentation Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# SDM.1.01 - System Documentation

## Control Statement

Documentation of system boundaries and key aspects of their functionality are published to authorized personnel.

## Context

This control formalizes the idea that we need to keep track of our production systems and maintain quality documentation for easy reference. Most of this control is naturally met by the emphasis we have on documentation here at GitLab. The remainder of this control is meant to ensure we are publishing any institutional knowledge about how systems interact and that we consider high-level system views as well as individual components.

## Scope

This control applies to all GitLab production systems.

## Ownership

TBD

## Guidance

The most common form of system documentation is network and data flow diagrams.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [System Documentation control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/873).

### Policy Reference

## Framework Mapping

* SOC2 CC
  * CC2.3
