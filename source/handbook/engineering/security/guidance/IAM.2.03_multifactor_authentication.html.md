---
layout: markdown_page
title: "IAM.2.03 - Multi-factor Authentication Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.2.03 - Multi-factor Authentication

## Control Statement

Multi-factor authentication is required for remote sessions access to environments that host production systems.

## Context

Where and only if applicable, appropriate, and technically feasible, and with the understanding GitLab is a cloud-native, multi-factor authentication will be enabled on user accounts accessing production systems.

## Scope

This control applies to all systems within our production environment. The production environment includes all endpoints and cloud assets used in hosting GitLab.com and its subdomains. This may include third-party systems that support the business of GitLab.com.

## Ownership

Control owner:
* Security Management

Process owner:
* Security Operations
* IT-Ops

## Guidance

Identity access management systems should enforce SSH or MFA for connections to production systems and networks.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Multifactor Authentication control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/814).

### Policy Reference

## Framework Mapping

* ISO
  * A.11.2.6
* SOC2 CC
  * CC6.6
  * CC6.7
* PCI
  * 8.1.1
  * 8.6
