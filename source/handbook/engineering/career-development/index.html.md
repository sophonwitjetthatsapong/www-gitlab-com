---
layout: markdown_page
title: "Engineering Career Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## The Three Components of Career Development

There are three important components of developing one's career:

### Structure

Team members who are (or want to be) on track for promotion should be engaged in
a career coaching conversation with their manager. Some basic information about
this process can be found in the [People Ops
handbook](/handbook/people-group/learning-and-development/#career-mapping-and-development).
Specific coaching plan templates are listed here to help start the conversation:

- [Senior
  Engineer](https://docs.google.com/document/d/11xZpY2RuTldp1g6bHRFYKwwlQjNjYaPquLPo5uD6hrg/edit#)
- (Others to come)

We want to build these documents around the career matrix for Engineering. Since this career
matrix is still being developed, these documents are currently based on the [job family requirements](/job-families/engineering/).

The first career matrix being developed is for [Individual Contributors in Engineering](career-matrix.html).

When a team member is deemed ready for promotion, their manager should follow
the [promotion process outlined by People
Ops](/handbook/people-group/promotions-transfers/).

Remember that the coaching process helps team members understand what they need
to do in order to prepare for a more senior position on the team. The promotion
process documents what an engineer has already done to deserve a more senior
position on the team. The two processes are related, but they are not
substitutes for each other.

### Individual Initiative

Most career opportunities involve stepping into a position of informal or formal leadership. As such, the initiative
of the individual is a necessary component, as well as a qualification. However, for the sake of inclusion we do ask that
managers periodically bring up the possibilities for advancement so that individuals know the avenues available to them
and receive encouragement to pursue them.  Managers and team members should strive to have a career development
conversation at least once a quarter.

### Opportunity

GitLab is a fast-growing startup. As such, there is no shortage of opportunity for advancement along either the
individual contributor or management tracks. We're fortunate that this is the easiest component. And internal
promotions are generally our first option.

Sometimes a position must become available on the management track before a promotion can occur. But at our
rate of growth in 2017 and 2018, it is usually only a matter of 6 months or less before something opens up for a prepared candidate.


## Individual Contribution vs. Management

Most important is the fork between purely technical work and managing teams. It's important that Engineering Managers self-select into that track and don't feel pressured. We believe that management is a craft like any other and requires dedication. We also believe that everyone deserves a manager that is passionate about their craft.

Once someone reaches a Senior-level role, and wants to progress, they will need to decide whether they want to remain purely technical or pursue managing technical teams. Their manager can provide opportunities to try tasks from both tracks if they wish. Staff-level roles and Engineering Manager roles are equivalent in terms of base compensation and prestige.

### Trying the Management Track

It's important that people interested in the management track have opportunities to try it out prior to committing themselves. Managers can provide multiple opportunities to Senior and Staff Engineers who want to consider moving into an available or upcoming manager role. Examples include hosting a Group Conversation, acting as the hiring manager for an intern position, or running a series of demo meetings for an important deliverable. These occasions for trying out management functions are useful and can provide good coaching opportunities for the individual looking to move to management. Some engineers may find through these experiences that they are better suited to remain on the technical path, and this gives them the ability to decide before investing a significant amount of time in the transition.

In order to facilitate this transition, we recommend any person moving from an Individual Contributor role to a Management role work with their manager to create a focused transition plan. The goal is to provide a concentrated look into the responsibilities and challenges of management with the understanding that, if the role is not for them, they can return to the IC track. A good example of how to build such a plan can be found in [this article](http://firstround.com/review/this-90-day-plan-turns-engineers-into-remarkable-managers/).  Another good resource for when you are ready for the critical career path conversation is [this 30 mins video](https://www.youtube.com/watch?reload=9&v=hMz6QDURQOM&list=PLBzScQzZ83I8H8_0Qete6Bs5EcW3p0kZF&index=6).

### Interim Management Positions

We create interim management positions when there is organizational need, and we imprint these in our company org chart. These may be filled by someone who is experimenting with the management track, or someone who is just filling in while we hire and who is not interested in pursuing management long term. This difference should be made explicit with the individual and team members before the interim role is created.

Regardless, when someone fills an interim role they are providing a service to the company, and perhaps getting a valuable career development opportunity for themself; so, poor performance against those interim duties would not result in termination. At worst, the person would return to their prior responsibilities. That does not mean, however, that and individual is immune to termination of their employment, if they commit some breach of their prior responsibilities or of the company's general guidelines while in an interim position. 

For Interim Managers who are pursuing the role long term: the expectation is that, before moving into the role full time, they will make at least one successful hire. The official promotion will not occur before 30 days after that person's hire, so that we can assess whether the hire was truly successful. If the new hire's success is indeterminate at the 30-day mark, then we will continue to review until a firm decision is made. If the new hire is not successful, that does not mean that the Interim Manager cannot eventually move into the full-time role.

Once the Interim Manager's first new hire has been at GitLab for 30 days the Interim Manager can submit a [promotion document](/handbook/people-group/promotions-transfers/#promotions--compensation-changes) to their manager and begin collaborating on a revised [Experience Factor Worksheet](/handbook/people-group/promotions-transfers/#promotions--compensation-changes). The goal of this process is to have a determination made by the interim manager's manager about the promotion to a permanent manager role within 30 days of the first successful hire into the new team.

Once you have been designated as the interim manager, the current manager should update all reports to in BambooHR with a job information change request and create an access level request to grant interim manager access in BambooHR. 

## Roles

#### Engineering

```mermaid
  graph LR;
  eng:jbe(Junior Backend Engineer)-->eng:ibe(Intermediate Backend Engineer);
  eng:ibe(Intermediate Backend Engineer)-->eng:sbe(Senior Backend Engineer);

  eng:sbe(Senior Backend Engineer)-->eng:stbe(Staff Backend Engineer);
  eng:stbe(Staff Backend Engineer)-->eng:dbe(Distinguished Backend Engineer);
  eng:dbe(Distinguished Backend Engineer)-->eng:ef(Backend Engineering Fellow);

  eng:sbe(Senior Backend Engineer)-->eng:em(Backend Engineering Manager);
  eng:em(Backend Engineering Manager)-->eng:de(Director of Engineering);

  eng:de(Director of Engineering)-->eng:sde(Senior Director of Engineering);
  eng:sde(Senior Director of Engineering)-->eng:vpe(VP of Engineering);

  eng:jfe(Junior Frontend Engineer)-->eng:ife(Intermediate Frontend Engineer);
  eng:ife(Intermediate Frontend Engineer)-->eng:sfe(Senior Frontend Engineer);

  eng:sfe(Senior Frontend Engineer)-->eng:feem(Frontend Engineering Manager);
  eng:feem(Frontend Engineering Manager)-->eng:de(Director of Engineering);

  eng:sfe(Senior Frontend Engineer)-->eng:stfe(Staff Frontend Engineer);
  eng:stfe(Staff Frontend Engineer)-->eng:dfe(Distinguished Frontend Engineer);
  eng:dfe(Distinguished Frontend Engineer)-->eng:feef(Frontend Engineering Fellow);

  click eng:jbe "/job-families/engineering/backend-engineer#junior-backend-engineer";
  click eng:ibe "/job-families/engineering/backend-engineer#intermediate-backend-engineer";
  click eng:sbe "/job-families/engineering/backend-engineer#senior-backend-engineer";
  click eng:stbe "/job-families/engineering/backend-engineer#staff-backend-engineer";
  click eng:dbe "/job-families/engineering/backend-engineer#distinguished-backend-engineer";
  click eng:ef "/job-families/engineering/backend-engineer#engineering-fellow";
  click eng:em "/job-families/engineering/backend-engineer#engineering-manager";
  click eng:de "/job-families/engineering/backend-engineer#director-of-engineering";
  click eng:sde "/job-families/engineering/backend-engineer#senior-director-of-engineering";
  click eng:vpe "/job-families/engineering/backend-engineer#vp-of-engineering";
  click eng:jfe "/job-families/engineering/frontend-engineer#junior-frontend-engineer";
  click eng:ife "/job-families/engineering/frontend-engineer#intermediate-frontend-engineer";
  click eng:sfe "/job-families/engineering/frontend-engineer#senior-frontend-engineer";
  click eng:stfe "/job-families/engineering/frontend-engineer#staff-frontend-engineer";
  click eng:dfe "/job-families/engineering/frontend-engineer#distinguished-frontend-engineer";
  click eng:feef "/job-families/engineering/frontend-engineer#frontend-engineering-fellow";
  click eng:feem "/job-families/engineering/frontend-engineering-manager";
```

#### Security Engineering

```mermaid
  graph LR;
  sec:se(Security Engineer)-->sec:sse(Senior Security Engineer);
  sec:sse(Senior Security Engineer)-->sec:stse(Staff Security Engineer);
  sec:sse(Senior Security Engineer)-->sec:sem(Security Engineering Manager);
  sec:sem(Security Engineering Manager)-->sec:ds(Director of Security);

  click sec:se "/job-families/engineering/security-engineer/#intermediate-security-engineer";
  click sec:sse "/job-families/engineering/security-engineer/#senior-security-engineer";
  click sec:stse "/job-families/engineering/security-engineer/#staff-security-engineer";
  click sec:sem "/job-families/engineering/security-management#security-engineering-manager";
  click sec:ds "/job-families/engineering/security-management#director-of-security";
```

#### Test Engineering

```mermaid
  graph LR;
  qual:jtae(Junior Test Automation Engineer)-->qual:itae(Intermediate Test Automation Engineer);
  qual:itae(Intermediate Test Automation Engineer)-->qual:stae(Senior Test Automation Engineer);

  qual:stae(Senior Test Automation Engineer)-->qual:sttae(Staff Test Automation Engineer);
  qual:stae(Senior Test Automation Engineer)-->qual:qem(Quality Engineering Manager);
  qual:qem(Quality Engineering Manager)-->qual:dqe(Director of Quality Engineering);

  click qual:jtae "/job-families/engineering/test-automation-engineer#junior-test-automation-engineer";
  click qual:itae "/job-families/engineering/test-automation-engineer#intermediate-test-automation-engineer";
  click qual:stae "/job-families/engineering/test-automation-engineer#senior-test-automation-engineer";
  click qual:sttae "/job-families/engineering/test-automation-engineer#staff-test-automation-engineer";
  click qual:qem "/job-families/engineering/engineering-management-quality/#quality-engineering-manager";
  click qual:dqe "/job-families/engineering/engineering-management-quality/#director-of-quality-engineering";
```

#### GitLab.com Support

```mermaid
  graph LR;
  sup:igsa(Intermediate GitLab.com Support Agent)-->sup:sgsa(Senior GitLab.com Support Agent);
  sup:sgsa(Senior GitLab.com Support Agent)-->sup:stgsa(Staff GitLab.com Support Agent);
  sup:sgsa(Senior GitLab.com Support Agent)-->sup:ssa(Services Support Manager);

  click sup:igsa "/job-families/engineering/dotcom-support/#intermediate-gitlabcom-support-agent";
  click sup:sgsa "/job-families/engineering/dotcom-support/#senior-gitlabcom-support-agent";
  click sup:stgsa "/job-families/engineering/dotcom-support/";
  click sup:ssa "/job-families/engineering/support-management#services-support-manager";
```

#### Support Engineering

```mermaid
  graph LR;
  supe:jse(Junior Support Engineer)-->supe:ise(Intermediate Support Engineer);
  supe:ise(Intermediate Support Engineer)-->supe:sse(Senior Support Engineer);

  supe:sse(Senior Support Engineer)-->supe:stse(Staff Support Engineer);
  supe:sse(Senior Support Engineer)-->supe:sem(Support Engineering Manager);

  supe:sem(Support Engineering Manager)-->supe:ds(Director of Support);

  click supe:jse "/job-families/engineering/support-engineer#junior-support-engineer";
  click supe:ise "/job-families/engineering/support-engineer#intermediate-support-engineer";
  click supe:sse "/job-families/engineering/support-engineer#senior-support-engineer";
  click supe:sem "/job-families/engineering/support-management#support-engineering-manager";
  click supe:stse "/job-families/engineering/support-engineer#staff-support-engineer";
  click supe:ds "/job-families/engineering/support-management#director-of-support";
```


## Internships

We are currently developing an internship program, but as of this date we do not
have an official program. When we come up with an official program, we will 
immediately update the handbook. In the past if you got a couple of merge requests
accepted, we'd interview you for a possible internship, but right now we are developing
something that is a better "fit" with our unique organization.

## Junior Engineers

Junior Engineers require a high degree of mentorship to effectively set them up for success. Because GitLab Engineering is scaling so quickly at the moment (100% in 2018, 125% in 2019) we are not hiring at the junior level. A good guideline for what it takes to meet our intermediate criteria is 2 years of professional experience with rapid growth. We encourage you to apply, even if you have questions.

GitLab is committed to increasing diversity of all types, particularly in leadership, as it's one of our [core values](/handbook/values/). There is a misapprehension that the junior role is an effective tool for increasing our diversity. GitLab Engineering has made its greatest gains in diversity since putting a moratorium on junior hiring. This indicates that diversity and the junior level are orthogonal, at best.

Before this level is re-opened we need to assess the following attributes:

* Is a structured, time-bounded internship program a better alternative to the junior level
* Either stop scaling as a whole, or specific teams must meet a criteria (size? maturity? composition?) to mentor a junior
* Change the process so junior positions are advertised for, rather than leveled on-the-fly
* Consider changing the name of the level, since "junior" is not an effective recruiting title
* How do we effectively mentor juniors at a remote-only company?
* There is a confidential item in line with our [not public guidelines](/handbook/values/#not-public)

## Senior Engineers

Note that we have a specific section for [Senior Engineer](/job-families/engineering/backend-engineer/#senior-backend-engineer) because it's an important step in the technical development for every engineer. But "Senior" can optionally be applied to any role here indicating superior performance. However, it's not required to pass through "senior" for roles other than Engineer.

Senior engineers typically receive fewer trivial comments on their merge requests. Attention to detail is very important to us. They also receive fewer _major_ comments because they understand the application architecture and select from proven patterns. We also expect senior engineers to come up with simpler solutions to complex problems. Managing complexity is key to their work. [Staff](/job-families/engineering/backend-engineer/#staff-backend-engineer) and [Distinguished](/job-families/engineering/backend-engineer/#distinguished-backend-engineer) positions extend the Senior Engineer role.


## Promotion

We strive to set the clearest possible expectations with regard to performance and [promotions](/handbook/people-group/promotions-transfers/). Nevertheless, some aspects are qualitative. Examples of attributes that are hard to quantify are communication skills, mentorship ability, accountability, and positive contributions to company culture and the sense of psychological safety on teams. For these attributes we primarily rely on the experience of our managers and the [360 feedback](/handbook/people-group/360-feedback/) process (especially peer reviews). It's our belief that while a manager provides feedback and opportunities for improvement or development, that it's actually the team that elevates individuals.

#### Transfer Options

The following table outlines the lateral transfer options at any level of the role. Experience factor might differ per individual to determine leveling for each of the positions listed.

| Starting Role       | Lateral Options           |
|---------------------|---------------------------|
| Frontend Engineer   | Product Designer          |
| Product Designer    | Frontend Engineer         |
| Backend Engineer    | Production Engineer       |
| Production Engineer | Backend Engineer          |
| Backend Engineer    | Support Engineer          |
| Support Engineer    | Backend Engineer          |
| Support Engineer    | Solutions Architect       |
| Support Engineer    | Implementation Specialist |
| Automation Engineer | Backend Engineer          |
| Backend Engineer    | Automation Engineer       |

Specific backend teams can also be looked at for a lateral transfer. Those teams include Distribution, Create, Verify, Release, Geo, Monitoring, Gitaly, etc.
