---
layout: markdown_page
title: "Engineering Career Framework"
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Engineering Career Framework

These are the expected behaviours of Engineers at GitLab.
**Most engineers at GitLab are Intermediate or Senior, so the priority is to address these levels first.**

Moving between levels, you should see that as engineers advance in their careers,
their duties should encompass a higher degree of complexity, the scope of their influence
should expand, and their contributions should become more impactful.

These behaviors should not be considered as a checklist for promotion. Excellent Engineers
may struggle with one or two aspects of their job and their strengths in certain areas should
make up for deficits in others. However, it is important to show a significant number of
strengths to be considered for promotion.

Each of these behaviors should be achievable inside of normal working hours. If you feel that
you need to work additional hours to be successful in your role you should talk to your manager.

Junior engineers are still included on this framework even though we are not actively hiring at this level.
Further discussion about junior engineers can be found on the [career development
page](/handbook/engineering/career-development/#junior-engineers).

### Glossary

These definitions are used for the scope of this document:

* _Team_ : [A manager and their reports](/company/team/structure/#team-and-team-members). For this document, this
can also be interpreted as "their product group".

### Scope of Influence

This refers to the range of people that you influence with your leadership and how broadly you are able to bring
people together to work on a common goal.

When reading this framework, it is important to consider the behavior within the context of the scope of influence
for the given level.

Juniors are expected to focus their energy on making sure that they are becoming proficient in their role.
They need to become self-reliant for the majority of their tasks so that they can start to help their team.

Moving towards a Senior role, an engineer's focus begins to extend beyond themselves and into their team. They
contribute to helping others on their team be successful and may start to develop a network that extends outside
of their team.

Staff engineers are actively participating in activities beyond the scope of their team, while Distinguished and
Fellow engineers may be interacting with departments outside that of Engineering.

As a result, the more senior you become, the greater impact, influence and contribution you will have. The results
that you generate and efficiencies you generate have the potential to impact a broader range of people.

## Values Alignment

In this section, we are considering how an engineer aligns with GitLab's values. We expect all engineers to adhere to
our values. As an engineer becomes more senior, they need to become custodians of our values and be vocal when they
see behaviour that does not align with them.

These are not described in great detail to avoid duplication with the [Values handbook page](/handbook/values/).

#### Collaboration

More details about Collaboration can be found on the [Values handbook page](/handbook/values/#collaboration).

#### Results

More details about Results can be found on the [Values handbook page](/handbook/values/#results).

#### Efficiency

More details about Efficiency can be found on the [Values handbook page](/handbook/values/#efficiency).

#### Diversity & Inclusion

More details about Diversity & Inclusion can be found on the [Values handbook page](/handbook/values/#diversity--inclusion).

#### Iteration

More details about Iteration can be found on the [Values handbook page](/handbook/values/#iteration).

#### Transparency
More details about Transparency can be found on the [Values handbook page](/handbook/values/#transparency).

<%= partial("career_matrix_table", :locals => { :category => "Values Alignment" }) %>

## Technical Competencies

Engineers must be able to produce high quality code that solves the requirement. With seniority comes
more complex tasks that are less well defined, and the need to help other engineers find good solutions.

#### Quality

Quality goes further than just testing. An engineer needs to deliver code of a high standard that is well written,
well tested, properly documented, maintainable, and achieves the original goal of the requirement. Juniors will need
more help than others to achieve this. As engineers become more experienced, they will achieve this consistently and
will in turn be able to help others produce quality code.

#### Complexity

As engineers become more experienced, they will be able to handle issues of increasing complexity with more ease. This
is very closely aligned with ambiguity.

Simple projects have no external dependencies, they can be built without very much specialist knowledge, and usually take
a short amount of time to complete. They may produce features of a high impact, but the work itself is low risk
and there is very little chance of something going wrong.

Another way to think about simple projects is that they do not cost as much as complex projects to execute.

Complex projects require coordination with many external dependencies, specialist knowledge is required to write the code,
and specialist knowledge is required to review the code. They may also have very vague requirements at the start. These
projects can take many months to deliver and carry a higher risk of failure.

#### Security

Engineers need to be able to fix security issues when they arise, and as engineers become more senior they need to
become more adept at preventing security issues from being introduced.

Security also encompasses the domain of compliance and more experienced engineers need to be aware of the compliance
implications of the requirements that they are asked to build, and be prepared to question these
requirements if necessary.

#### Technical Stewardship

Stewardship refers to caring for the code base, looking out for how changes are made, and considering how new work
fits within the existing classes and modules of the system.

As an engineer becomes more senior, they start to describe the direction that the codebase needs to take.

#### Performance

This category is concerned with how an engineer's code performs in a production environment. This includes looking at
the resources required to run the features that are in the engineer's domain. Some examples are processing power,
network resources, or disk space.

Junior engineers will be looking at their work and checking it's performance, and at more experienced levels, these
engineers will need to verify the performance of the code that is inside of their domain.

#### Open Source

With the nature of GitLab being open source, we want to encourage engineers to be part of the open source community.

<%= partial("career_matrix_table", :locals => { :category => "Technical Competencies" }) %>

## Leadership Competencies

Leadership is not the same as Management. Both individual contributors and managers need to exhibit good leadership.
Management is more concerned with making sure that projects are successful and that teams form good working relationships
together. Leadership is about providing a clear and compelling direction to follow. These categories look at
leadership from the IC's perspective.

#### Growing Others

Here we are looking at how an engineer helps others to grow. This can be by giving fair and kind code reviews, providing
timely feedback, or more formally - through mentorship. "Others" in this case can refer to team members, other engineers,
community members, or even other GitLabbers outside of the Engineering function.

#### Communication

This ranges from managing expectations with your manager and your team to finding simple and easy ways to explain
technical concepts to non-technical people that interact with your team. Communication becomes more nuanced as your
scope of influence changes because your voice carries to a broader range of people.

#### Ambiguity

At the start of an engineer's career, requirements are well constructed and the problem to solve is clearly articulated. As
they become more senior, they need to be more comfortable when requirements are less clearly defined and when the problem
itself may not be well understood. The most senior engineers may even be in a position where they know that something is wrong,
but they are not exactly sure what it is - and they work to define the problem.

#### Business Acumen

This category looks at how an engineer views their work in the context of the goals of the business and the
direction of the company.

#### Process

Here we look at how engineers view the processes that they need to operate within.


<%= partial("career_matrix_table", :locals => { :category => "Leadership Competencies" }) %>

## Download In CSV Format

This framework is available
for <a href="career_matrix_csv.csv" download="engineering_ic_career_matrix.csv">download in a csv format</a>.