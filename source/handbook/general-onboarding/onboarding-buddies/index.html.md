---
layout: markdown_page
title: "GitLab Onboarding Buddies"
---

----

## On this page
{:.no_toc}

- TOC
{:toc}
----

## Onboarding Buddies

Onboarding buddies are crucial to making the onboarding experience for a new GitLab team-member a positive one. [New Job Anxiety](http://www.classycareergirl.com/2017/02/new-job-anxiety-conquer/) is a reality for many people, and the adjustment to GitLab might be particularly challenging for new GitLab team-members who may not be used to our [all-remote](/company/culture/all-remote/) culture. That's why it's important that all new GitLab team-members be assigned a buddy who is ready, willing, and excited to assist with the onboarding process.


## Buddy Responsibilities

1. **The first and most important thing an onboarding buddy should do is schedule a call with the new GitLab team-member.** We attempt to match (as best as possible, anyway) time zones between the new GitLab team-member and their onboarding buddy so that as soon as the new GitLab team-member logs on, you, the onboarding buddy, can be there ready and waiting to welcome them to the team.
1. **Check how far the new GitLab team-member has gotten in their onboarding issue.** The onboarding issue that all new GitLab team-members are assigned can be overwhelming at first glance, particularly on the first day of work. Check to see how much, if any, the new GitLab team-member has done by the time your call happens, and offer some direction or advice on areas the new hire may be having trouble with.
1. **Suggest helpful handbook pages.** Chances are that you've discovered some particularly helpful pages in the handbook during your time at GitLab. Point them out to the new GitLab team-member, and help them get used to navigating the handbook. Some examples might include:
     * [The tools page](/handbook/tools-and-tips)
     * [The team chart](/company/team/org-chart)
     * [The positioning FAQ](/handbook/positioning-faq)
1. **Remind them about introducing themselves.** Remind the new team member to introduce themselves in the Slack channel `#new_labbers`. Encourage them to write a little personal note, and if they're comfortable, include a photo or two!
1.  **Encourage them to organize a group call with other new hires.** New GitLab team-members who are used to (or prefer) a more conventional new hire orientation — frequently hosted in group settings in colocated organizations — [may feel a lack of early bonding](/company/culture/all-remote/learning-and-development/). Encourage them to organize a group call with other new hires in order to walk through onboarding together, while learning about new personalities and [departments of the company](/company/team/structure/).
1. **Introduce them to Slack.** Slack may seem like it's ubiquitous, but that doesn't necessarily mean the new GitLab team-member will have had experience using it before. Since it's a central part of how we communicate at GitLab, consider showing them around, and give them some pointers about [how we use it](/handbook/communication/#chat). 
  
     * Be sure to suggest [location channels](/handbook/communication/chat/#location-channels-loc_) (e.g. `#loc_australia`, `#loc_denver`, etc.) and [Social Slack Groups](/handbook/communication/chat/#social-groups) (e.g. `#intheparenthood`, `#remote`, `#travel`, `#fitlab`, `#cats`, `#dog`, `#cute-photos`, `#mac-power-users`, `#gaming`, etc.,) where they can immediately plug in with other team members who appreciate similar things.
1. **Ask where they need help and connect them with the experts**. Onboarding buddies should make the effort to connect new GitLab team-members with subject matter experts if your assigned team member requests additional help in a given area. Examples are below.
     * For new GitLab team-members who have not worked in a [remote organization](/company/culture/all-remote/) before, they may need assistance in thinking through an ideal [workspace](/company/culture/all-remote/workspace/) and embracing [informal communication](/company/culture/all-remote/informal-communication/). Consider asking seasoned remote colleagues in the `#remote` Slack channel to reach out and answer questions.
     * If they're new to [Git](/training/), consider asking experts in the `#git-help` Slack channel to reach out and offer a tutorial.
     * If they're new to [Markdown](/2018/08/17/gitlab-markdown-tutorial/), consider asking experts in the `#content` Slack channel to reach out and offer support.
1. **Help with the team page.** For less technical new hires, adding themselves to the [team page](/company/team/) might feel like the most daunting task on the onboarding issue. Offer to help with the process. This doesn't necessarily have to happen on day one, but you should let them know that you're available to help if and when they need it. Consider scheduling a second meeting later in the week to walk them through [the process](/handbook/git-page-update/#11-add-yourself-to-the-team-page)
  * In particular, help them to create their SSH key as this tends to be a sticking point for many new hires. You can also show them [Lyle's walkthrough](https://youtu.be/_FIOhk03VtM).
1. **Check in regularly.** You may very well be the first friend the new GitLab team-member makes on the team. Checking in with them regularly will help them feel welcome and supported. Reach out via Slack, and schedule at least one follow-up call for the week after their start date.
1. **Provide backup if needed**. If you plan to be out (e.g. [vacation](/handbook/paid-time-off/), [company business](/handbook/travel/), [events](/events/), etc.) during a new GitLab team-member's first few weeks, please ensure that a backup onboarding buddy is available to offer support.

## That's it!

That's all there is to it! Thanks for your help welcoming the newest GitLab team-member to the team and getting them on board. If you have questions that are not answered on this page, please [reach out to People Ops](/handbook/people-group/)!
