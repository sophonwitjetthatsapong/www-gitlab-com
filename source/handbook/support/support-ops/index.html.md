---
layout: markdown_page
title: Support Engineer Ops
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Support Operations team maintains the day-to-day operations and software systems used by GitLab’s global Technical Support team.


## Customer Satisfaction Survey (CSAT)

At GitLab, CSAT is a measure of how satisfied our customers' are with their interaction with the GitLab Support team. This is based on survey responses from customers after each ticket is solved by the Support team using a Good or Bad rating.

The target is 95% customer satisfaction. The definition of the KPI is:

```
Satisfied [total good scores]  / Total Survey Responses [good scores + bad scores]
```

This calculation excludes cases where a survey was not offered or where it was offered but no score was provided.

The survey is sent out 24 hours after a ticket is solved. The tag `csat_survey_sent` is added to a ticket as soon as a survey is sent out. 

#### CSAT in numbers

- Our average response rate is 24%.
- In average, 90% of our tickets receive a CSAT Survey.

There are a few reasons why a customer wouldn’t receive a CSAT Survey after a ticket has been solved:

- Ticket was merged into another ticket, and subsequently closed (tag `close_by_merge`).
    - If a customer follows up on a ticket that was closed by merge, the tags of the original ticket are copied over to the new ticket, therefore not all tickets which contain this tag haven't been sent the CSAT Survey.
- Ticket was received through mail-list@gitlab.com (tickets received through this channel are automatically closed).
- Ticket was submitted by a member of the GitLab team (‘GitLab’ Organization). If a member of other GitLab Zendesk organizations submits a ticket, they will receive a survey (ie GitLab Inc., GitLab BV, GitLab Ltd).

#### Managing Negative Feedback

Starting July 9th, 2019, in order to learn about the reasons behind a bad satisfaction rating, we ask any customer who gives a negative survey response to select a reason for their dissatisfaction. This feedback is shared with the Product team and the Support Managers, depending on which reason is chosen. The dropdown options are:

- GitLab doesn't meet my needs (Product Team)
- The answer wasn't delivered in a timely manner (Support Managers)
- My issue was not resolved (Support Managers)
- The answer wasn't helpful (Support Managers)
- Some other reason

All negative and positive feedback with comment is exported to an internal repository. Once the feedback has been submitted, an Issue is created and the Support Engineering Managers are tagged when a negative rating has been submitted. It is the responsibility of the Support Engineering Managers to investigate the feedback and ensure appropriate actions are taken to either resolve the root cause of the negative experience or reduce the likelihood of it recurring.

## Shared Organizations in Zendesk

We have the option of allowing all of the end-users in an organization to see each other's tickets. This is referred to as a shared organization in Zendesk.

We can grant the end-users permissions to see the tickets at the organization level and the personal level (personal level overrides org level). This means that at the personal level, we can allow selected individuals in an org to access all the tickets created by members of their org or we can allow everyone in the company to see everyone's tickets.

Shared organizations will only be set up if we receive a request directly from the customer through a Support Ticket. Once the ticket is created, any member of the Customer Success or Support teams can create a request in the [Support Ops project](https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/issues/new?issuable_template=Shared%20Organization%20Request) using the `Shared Organization Request` template, and any member of the Support Team can update the organization settings once the issue is created. 

There are a few different options available for our customers:

- Users can view and edit their own tickets only
- All users can view all tickets but not add comments
- All users can view all tickets and add comments to all tickets
- Only selected users can view all org's tickets
- Only selected users can view and comment on all org's tickets 

## Salesforce - Zendesk sync

### Ticket sending to Salesforce

We have enabled Zendesk to send all ticket information to Salesforce using a target. Currently, we create a  Salesforce Zendesk Ticket record whenever a ticket is created and when it is solved by one of our agents or an automation. Once the information is received in Salesforce, a notification is sent from SFDC to the Technical Account Managers so they are aware of the issues their accounts are experiencing. 

To troubleshoot any issues with this functionality, we need to make sure that:

- The [Salesforce target and trigger in Zendesk](https://support.zendesk.com/hc/en-us/articles/203660006-Salesforce-Setting-up-sending-Zendesk-tickets-into-Salesforce/#ariaid-title3) are properly configured.
- The [matching criteria for ticket requester and ticket organization](https://support.zendesk.com/hc/en-us/articles/203660006-Salesforce-Setting-up-sending-Zendesk-tickets-into-Salesforce/#ariaid-title2) is set to `Use auto-matching criteria for searching Salesforce records`
- The Field Level Security for the Objects `Zendesk Support Ticket` and `Zendesk Support Ticket Comment` is visible to the `System Administrator`

When working on the Salesforce side of this configuration, make sure to always communicate with the Sales Systems and Operations team about any inconsistencies you find and request their help to troubleshoot any issues.

### Account - Organization sync from Salesforce

All organizations in Zendesk are created through our Salesforce Integration. Currently, only records with Account Type equal to `Customer` are synced from Salesforce to Zendesk. 

Accounts are synced with Organizations as long as the `Account ID` in SFDC matches the one `Salesforce ID` in Zendesk and the `Zendesk Support Organization ID` in SFDC matches the `Zendesk Organization ID` in Zendesk. 

We have configured custom field mappings: 

- Account Owner 
- ARR 
- Salesforce ID
- Number of Seats
- Market Segment
- Support Level
- Technical Account Manager

A script was created by the Support Team in order to do [routine checks of data integrity between Salesforce and Zendesk](https://gitlab.com/gitlab-com/support/support-ops/crm-check). This script runs daily and it checks for:

-  Salesforce Accounts that don't have a Zendesk Organization for GitLab.com Customers (`missingzendeskorg_dotcom.rb`) and Self-Managed Customers (`missingzendeskorg_selfmanaged.rb`)
- Zendesk Organizations with no GitLab Plan (`nosupportlevel`)
- Duplicate customer records in Salesforce (`repeatrecordsinsalesforce`)
- GitLab Plan mismatch between SFDC and ZD records(`supportlevelmismatch`)
- Zendesk Organizations with no Salesforce ID (`nosalesforceid
`)

## Other tools
{: #other-tools}

### Slack notifications

#### Premium Breach Notifications
{: #premium-breach-notifications}

The premium breach notifications is a webhook that is triggered by a ZenDesk automation titled `Premium important ticket slack notification`. The trigger looks for any
high premium tagged tickets that have less than 2 hours left on the breach clock.
The automation [runs at the top of every hour](https://support.zendesk.com/hc/en-us/articles/203662236-About-automations-and-how-they-work)
and due to this limitation, it does not run at the exact hour the ticket is less than 2 hour from breaching.

Once the ticket is updated, a trigger sends a webhook to slack which is configured on the slack side.

#### #zd-self-managed-feed and #zd-gitlab-com-feed 
{: #zd-self-managed-feed}

The `#zd-self-managed-feed` channel is updated by a webhook that's triggered whenever a Self-Managed ticket is created or updated. The Zendesk trigger is called "Slack Support Live Feed (With Organization names)"
The `#zd-self-managed-feed` channel does not display tickets addressed to GitLab.com or
GitHost.io. 

The `#zd-gitlab-com-feed` channel is updated by a webhook that's triggered whenever a GitLab.com ticket is created or updated. The Zendesk trigger is called "Slack Services Live Feed". This channel does not display EE tickets.

#### Support Bot

The [Supportbot](https://gitlab.com/gl-support/gitlab-support-bot/tree/master/) shares status of Zendesk views and number of tickets in Slack when invoked. 
