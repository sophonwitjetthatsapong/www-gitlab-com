---
layout: markdown_page
title: "Professional Services Engineering"
---
# Professional Services Engineering Handbook
{:.no_toc}

The Professional Services Engineering group at GitLab is a part of the [Customer Success](/handbook/customer-success) department. Professional Services Engineers have the goal to optimize organization's adoption of GitLab through our professional services, designed to enable other necessary systems in your environment so that customers can move from planning to monitoring.

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

<div class="alert alert-purple center">
    <h3 class="purple">
        <strong>
            Accelerate our customer’s DevOps Transformation <br/>
            by providing services to improve efficiency, time to market and agility <br/>
            through GitLab product adoption
        </strong>
    </h3>
</div>

## Strategy

Accelerate time to value to lead adoption and support product revenue growth

### Why GitLab Professional Services?

We have chosen to have a Professional Services offering at GitLab for several reasons, including benefits to both GitLab and our customers.

#### Benefits to our customers
Having a Professional Services team benefits our customers in the following ways:

* Faster value realization
* Improved customer experience
* Access to GitLab experts / best practices

### Benefits to GitLab 
Having a Professional Services team benefits GitLab and our [business model](/monetizing-and-being-open-source/) and [strategy](/company/strategy/) in the following ways:

* Improved retention and expansion
* Customer insights and feedback
* Experience and offers for partners

### How will we deliver?
We plan to deliver on our Vision and Strategy leveraging two methods:

1. Direct to customers delivered via [GitLab team-members](/company/team/?department=professional-services) as described in our [professional services offerings](#professional-services-offerings)
1. Partner delivered to allow us to leverage partners in order to: 
  - Ensure we have local coverage globally
  - Complete services offerings
  - Create a partner revenue stream
  - Partner delivered services
  - Contracted delivery resources
  - Allow us to deliver and develop IP


## Role & Responsibilities
See the [Professional Services Engineer role description](/job-families/sales/professional-services-engineer/)

### Statement of Work creation

The GitLab Professional Services Engineering group is responsible for maintaining the [GitLab SOW Calculator](https://services-calculator.gitlab.io).  The production of new Statements of Work for customer proposals is owned by the [Solutions Architects](/handbook/customer-success/solutions-architects/).

To obtain a Statement of Work, start with an SOW from the [Services Calculator](https://services-calculator.gitlab.io) which will open a new SOW issue on the [SOW Proposal Approval board](https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/boards/1353982?label_name[]=Services%20Calculator).  Follow the specific [SOW process](/handbook/customer-success/professional-services-engineering/selling/#selling-services-workflow) for a complete explanation. 

We prefer customers to mark up our agreement and SOW template if they request changes. If they require the use of their services terms or SOW, please contact the Professional Services Engineering group.

#### SOW Proposal Approval board

![SOW proposal board screenshot](/handbook/customer-success/professional-services-engineering/sow-approvals-board.png)

The [SOW Proposal Approval board](https://gitlab.com/groups/gitlab-com/customer-success/professional-services-group/-/boards/1353982?label_name[]=Services%20Calculator) is used to approve all Statements of Work (SOW)s before sending them to a customer.  For the entire process, see the [SOW process](/handbook/customer-success/professional-services-engineering/selling/#selling-services-workflow).

The labels are (from left to right):

* `Open`: This issue has been created by the Services Calculator and waiting for the SA to confirm
* `proposal::Scoping`: Gathering information from the account team and customer required to scope the engagement
* `proposal::Writing`: The Professional Services team is finalizing the terms of the SOW
* `proposal::ReadyForApproval`: The Professional Services team has completed the SOW, and it is ready to be approved by the VP of Customer Success
* `proposal::Approved`: The SOW is ready to be sent  to he customer by the account team.  Once sent, this issue can be closed

NOTE: Issues that have not been updated by a Solutions Architect in the last 30 days will be automatically closed.

### Scoping Custom Engagements 

The [two ways](#selling-professional-services) services are sold are: out of the box SKUs or a Statement of Work from the [Services Calculator](http://services-calculator.gitlab.io/) that may need customization.  When scoping custom SOWs, the Professional Services team partners with the SA to create a custom engagement that will help solve the customer's needs.

To understand the workflow for custom SOWs, read the [custom SOW workflow](/handbook/customer-success/professional-services-engineering/selling/#selling-services-workflow).

For scoping questions to help uncover what is required to scope a custom engagement see [custom SOW scoping details](/handbook/customer-success/professional-services-engineering/scoping/).

### Long Term Profitability Targets
The long term gross margin target for services is 35%.

### Services Attach Rate for Large Accounts
The percent of Large Parent Accounts that have Professional Services associated with it or any linked SFDC Accounts. GitLab's target is above XX%.

### Services Delivery

#### Implementation Plan

Each services engagement will include an Implementation Plan compiled based on the Statement of Work and other discussions the Professional Services Engineering group has with the customer.  The Professional Services Engineering group also maintains the SOW template located [at this internal link](https://docs.google.com/document/d/1X8_EiX8kgJdpaVlydbTJg5pn4RXeDvYOIyok2G1A69I/edit).

Each services engagement will have a google sheet that shows the contracted revenue and the estimated cost measured in hours or days of effort required to complete the engagement.  The cost estimate must be completed prior to SoW signature and attached to the opportunity in SFDC.

### Professional Services Engineering Workflows

For details on the specific Professional Services Engineering plays, see [Professional Services Engineering workflows](/handbook/customer-success/professional-services-engineering/workflows).

## Selling Professional Services

Services can be sold via:
* A **[out-of-the-box SKU](#professional-services-skus)**
* Through a **Statement of Work (SOW)** from the [Services Calculator](http://services-calculator.gitlab.io/).  These may be customized by following the [SOW workflow](/handbook/customer-success/professional-services-engineering/selling/#selling-services-workflow)

For more details on selling professional services, see [Selling Professional Services](/handbook/customer-success/professional-services-engineering/selling).

### Lead time for starting

Often we are asked "what is your lead time for getting started with a project" or a customer may have a specific timeframe in which they want a project delivered.

As there may be many projects and proposals in flight at any time, there are a few rules we use when prioritizing and scheduling engagements:

* No engagement schedule can be committed to until a signed SOW is received.  
* While we can make a good faith effort to try and match a schedule ahead of that time, to be fair to customers who have committed to a services engagement we must prioritize those first. 
* Our typical lead time for starting an engagement is 4 weeks or less, but this can vary greatly as we scale the team and engagements.  
* Please check with the Professional Services Manager for the latest lead time details in #g_professionalservices if you need a more accurate estimate.
* If your customer has a specific need, please discuss with the Professional Services Manager - either in the issue for the SOW or in #g_professionalservices - before committing to anything for customer

## Professional Services Offerings

### Hierarchy

1. **Levels**: There are three levels of service that represent the different services buyer personas.
1. **Categories**: Each level has one or many categories of service that define a set of related services either by type or product area.
1. **Offering**: Each category has one or many offerings that is a single consumable unit that has all of the required pieces to make a complete service offering.
1. **Offering Variants**: Each offering may have one or many variants that allow it to be deployed in different ways.  

Each offering at least has a variant called “standard” or “custom” to define if it can be delivered with a standard SKU / out of the box SOW. For example, enterprise versus commercial or remote versus on-site or one-time versus with an embedded engineer.

### Levels of Service
There are three levels of service we talk about when it comes to classifying our service offerings.

#### Product Services
**Installing, scaling & using the tool(s)**

Product services comprise services geared toward GitLab the software itself.  Getting a customer up and running with GitLab in a secure, highly-available environment to ensure their success with the application.

For example:
* Implementation & Migrations
* Training & Certification
* Performance tuning & advanced administration

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

**Planned Offering Maturity**

![product services slide 1](/handbook/customer-success/professional-services-engineering/product_services1.jpg)

![product services slide 2](/handbook/customer-success/professional-services-engineering/product_services2.jpg)

Note - some offerings here represent future plans.  They will be turned into dynamic items using `services.yml` to remove the need for a screenshot.

#### Operational Consulting Services
**Processes aligned with value-added changes**

Operational Consulting Services help teams on the path towards full DevOps maturity adopt industry best-practices and advanced use cases of GitLab to accelerate their time to market.  These services live one level above Product Services in the sense that they are less focused on the tool.  Operational Consulting Services are more focused on the processes in place that helps make the tool a success across a large enterprise.

For example:
* Development & DevOps process consulting
* Increasing DevOps maturity
* Establishing & measuring cycle time

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

**Planned Offering Maturity**

![operational consulting services slide 1](/handbook/customer-success/professional-services-engineering/operational_services1.jpg)

![operational consulting services slide 2](/handbook/customer-success/professional-services-engineering/operational_services2.jpg)

Note - some offerings here represent future plans.  They will be turned into dynamic items using `services.yml` to remove the need for a screenshot.

#### Strategic Consulting Services
**Adapt people’s way of thinking to the new paradigm**

Strategic Consulting Services live one level above Operational Consulting Services as they focus on organization changes and changes to the people and their behavior.  These changes are required to get the full value from a DevOps transformation.  We will provide some services in this area though the complete offering will be provided in collaboration with our partner ecosystem.

For example: 
* Cultural transformation
* Executive coaching
* Organizational structure consulting
* Digital strategy

See [professional services offerings](/handbook/customer-success/professional-services-engineering/offerings) for a detailed listing of offerings.

**Planned Offering Maturity**

![strategic consulting services slide 1](/handbook/customer-success/professional-services-engineering/strategic_services.jpg)

Note - some offerings here represent future plans.  They will be turned into dynamic items using `services.yml` to remove the need for a screenshot.

### Offering Maturity Model
The services maturity framework provides for 5 maturity levels for offerings: planned, minimal, viable, complete and lovable.

* **Planned**: A future planned offering
* **Minimal**: The offering is defined, a vision for moving to complete exists 
* **Viable**: We have delivered the offering at least once, feeding lessons learned into completion plan. At least some marketing materials and execution plans from Complete
* **Complete**: An offering that can be consistently delivered: predictability in timing, results, and margin. See [full definition of complete](#complete-offering) below.
* **Loveable**: The offering is at full maturity, positive NPS & impact on customer’s adoption of GitLab product

#### Complete Offering
Required Items for Complete maturity:

* Marketing page on /services
* Collateral for Sales org
  - Description of Services
  - Slides
  - Training: personas, learning goals
* Pricing
* Sales Enablement Training
  - e.g. a video or other sales enablement for SAs & SALs
* Clear definition of the breadth & depth of our offering
* Included in [Services Calculator](https://services-calculator.gitlab.io/)
* If applicable, **Standard variant** with
  - Standard SOW
  - Finalized SKU for use by Sales
* Custom SOW template
* Clear goals and objectives for offering
  - Education: Learning objectives
  - Other types: Measuring outcomes
* Engagement Execution Plan
  - Expectations, timing, process

### Professional Services SKUs
Some services, such as training, quick-start implementation, and other out-of-the-box implementation packages can be sold as a [SKU](#professional-services-skus) which has a fixed price and fixed SOW associated.

#### Current SKUs
Currently, the following services can are sold as a SKU standard offering.  These offerings can be added to any quote, and reference a standard SOW. There is no need for the customer to sign a custom SOW in order to order these.

<%= partial("skus_table") %>

#### Creating a new Professional Services SKU
To create a new SKU, the following are the requirements:

* A specimen SOW that can be referenced by the Item
* Established cost-basis and assumptions
* Hours and hourly cost
* Delivery cost (hours * hourly cost)
* Included T&E assumption (if any)

After those requirements are met, the process to create a new SKU is:

1. Create an [issue in the Finance issue tracker]() referencing the above requirements
1. Review with the Finance Business Partner for Sales
1. Make any require changes
1. Submit to VP of Customer Success for approval
1. Submit to CFO, CRO and CEO for approval
1. Once approved, submit to Accounting to create SKU in Zoura
1. Once SKU is created, add to `Current SKUs` above

## How to work with/in the Professional Services Engineering group

### Contacting & Scheduling

At GitLab, Professional Services Engineering is part of the [Customer Success department](/handbook/customer-success).  As such, you can engage with Professional Services Engineering by following the guidelines for [engaging with any Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).  This process ensures that the Customer Success department as a whole can understand the inbound needs of the account executive and our customers.

For scheduling specific customer engagements, we currently are slotting implementations while our group grows to support the demand for services.  If you have a concern about scheduling the engagement, this should be discussed at the Discovery phase.  In no case should you commit to dates before receipt of agreements, P.O., etc.

### Contacting Professional Services Engineering

To contact the Professional Services Engineering group, the best way is to follow the guidelines for [Engaging a Solutions Architect](/handbook/customer-success/solutions-architects#when-and-how-to-engage-a-solutions-architect).

You can also reach the group via the [#customer-success Slack Channel](https://gitlab.slack.com/messages/C5D346V08/).

### Professional Services Engineering Issue Board

The [Professional Services Engineering Issue Board is available here](https://gitlab.com/gitlab-com/customer-success/professional-services-group/ps-plan/boards).  This board contains everything that the group is working on - from strategic initiatives to [SOW writing](#statement-of-work-creation), all group activity is available here.

#### Time Tracking

The professional services team will track billable hours within GitLab against each Statement of Work ("SOW") for internal use regardless of the billing structure agreed upon with the customer (such as fixed bid or time and materials). Detailed and accurate time tracking records of billable hours is essential to ensure revenue can be recognized based upon percentage completion of the project scope as outlined in the SOW, and this data is used in the calculation of gross margin. 

Billable hours represent work hours that a staff member reports as being aligned to a specific SOW. The format for daily time tracking for each team member is shown below, and should be reviewed by the professional services leadership for approval and signoff before being submitted each Monday (for the week prior) to the Finance Business Partner for Sales. Rounding to the nearest hour is acceptable, and please ensure data is recorded in numeric format without the use of text strings such as "hrs". 

| Manager Approved Y/N | Finance Accepted Y/N |
| ------------- |-------------|


| Customer Name | SOW#  | mm/dd/yy (Day 1) | mm/dd/yy (Day 2)  | mm/dd/yy (etc...)  | Total Billable Hrs  |
| ------------- |:-------------:| -----:|-----:|-----:|-----:|
| Customer 1     | SOW 1 | X.X |X.X|X.X|X.X| 
| Customer 2     | SOW 2     | X.X |X.X|X.X|X.X|  
| Customer 3 | SOW 3     |  X.X |X.X|X.X|X.X|



#### Project Completion

At the conclusion of the Statement of Work the Professional Services Engineer will prepare a cost vs actual analysis. This analysis will be filed in SFDC.  When filed in SFDC the Professional Services Engineer will @mention the the Controller and Finance Business Partner, Sales in SFDC Chatter.

#### SOW Issues

When requesting a SOW, Account Executives or Professional Services Engineering group members should use the SOW issue template, which automatically shows the required information as well as labels the issue appropriately.

#### Strategic Initiatives
Strategic Initiatives are undertaken by the Professional Services Engineering group to leverage team member time in a given area of the Customer Success Department.  This can include increased documentation, better training resources or program development.

