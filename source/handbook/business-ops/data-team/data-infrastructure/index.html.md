---
layout: markdown_page
title: "Data Infrastructure"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## System Diagram

```mermaid
graph LR

  subgraph "Snowflake Data Warehouse "

    subgraph "Analytics DB "
      Analytics_Sensitive[Analytics Sensitive Schema ]
      Analytics_Staging[Analytics Staging Schema ]
      Analytics[Analytics Schema ]
    end
    SnowflakeRaw[Raw DB ] -- dbt --> Analytics_Sensitive
    SnowflakeRaw[Raw DB ] -- dbt --> Analytics_Staging
    SnowflakeRaw[Raw DB ] -- dbt --> Analytics
    SnowflakeRaw -- dbt snapshots  --> SnowflakeRaw
    SnowflakeRaw -- Roles Snapshot  --> SnowflakeRaw
    SnowflakeRaw -- Snowplow Event Sample  --> SnowflakeRaw
  end

  subgraph "Periscope "
    Analytics-- Queries --> Periscope_data(Periscope Data )
    Analytics_Staging -- Queries --> Periscope_data(Periscope Data )
  end

  subgraph "Stitch "
    StitchData(Stitch ) --> SnowflakeRaw
  end

  subgraph "Airflow "

    subgraph "Other DAGs "
     AirflowDAGs(Other DAGS) --> SnowflakeRaw
    end

    subgraph "Postgres Pipeline "
      PostgresPipeline(Postgres Pipeline) --> SnowflakeRaw
    end
    
    subgraph "Sheetload "
      Sheet_Load(Sheetload ) --> SnowflakeRaw
    end

  end

  subgraph "Internal Data Sources "
    CloudSQL[Airflow CloudSQL ] --> StitchData
    CI_Stats[CI Stats CloudSQL ] --> PostgresPipeline
    GitLab_Profiler[GitLab Profiler CloudSQL ] --> PostgresPipeline
    YAML(YAML Extracts ) --> AirflowDAGs
    Engineering(Engineering Commit Extracts ) --> AirflowDAGs
    subgraph "Internal Azure VM "
      VersionDB[Version DB ] --> PostgresPipeline
      CustomersDB[Customers DB ] --> PostgresPipeline
      LicenseDB[License DB ] --> PostgresPipeline
    end
  
  end
  
  subgraph "AWS S3 "
    Greenhouse_S3(Greenhouse Bucket ) --> Sheet_Load
    Snowplow(Snowplow Bucket ) -- Snowpipe --> SnowflakeRaw
  end
  
  subgraph "Third-Party Data Sources"
    Bamboo(BambooHR ) --> AirflowDAGs
    Greenhouse(Greenhouse ) --> Greenhouse_S3
    Zuora(Zuora ) --> StitchData
    Zendesk(Zendesk ) --> StitchData
    SalesForce(SalesForce ) --> StitchData
    Netsuite(Netsuite ) --> StitchData
  end

  subgraph "GitLab  Dotcom "
    GitLab_dotcom[Postgres ] --> PostgresPipeline
    GitLab_snowplow(Snowplow Events ) -- Snowplow Infra --> Snowplow
  end

  subgraph "Google Sheets "
    Google_Sheets(Google Sheets ) --> Sheet_Load
  end
```

## Airflow

We use Airflow for Orchesetration, and this includes testing in MRs. The following is the new MR workflow that includes testing with Airflow.

### Airflow in Production ([Data Infrastructure repo here](https://gitlab.com/gitlab-data/data-image?nav_source=navbar))

All DAGs are created using the `KubernetesPodOperator`, so the airflow pod itself has minimal dependencies and doesn't need to be restarted unless a major infrastructure change takes place.
There are 4 containers running in the current Airflow deployment:

1. A sidecar container checks the repo activity feed for any merges to master. If there was one, the sidecar will reclone the repo so that Airflow runs the freshest DAGs.
2. The Airflow scheduler
3. The Airflow webserver
4. A cloudsql proxy that allows Airflow to connect to our cloudsql instance

#### Handling Failed Airflow Jobs

There should never be more than one failed DAGrun visible for any DAG at one time. For incremental jobs that rely on the `execution_date`, any failed DAGs need to have their task instances cleared so that they can be rerun once the fix has been applied.
For jobs that are not dependent on `execution_date` the job should be rerun manually when the fix is applied and the failed DAGrun(s) should be deleted. If there is a failed DAGrun for a DAG it should mean that the current state of that DAG is `broken` and needs to be fixed.
This will make it easier to glance at the list of DAGs in Airflow and immediately know what needs attention and what doesn't.

### Airflow in MRs

To facilitate the easier use of Airflow locally while still testing properly running our DAGs in Kubernetes, we use docker-compose to spin up local Airflow instances that then have the ability to run their DAG in Kubernetes using the KubernetesPodOperator.
The flow from code change to testing in Airflow should look like this (this assumes there is already a DAG for that task):

1. Commit and push your code to the remote branch.
1. Run `make init-airflow` to spin up the postgres db container and init the Airflow tables, it will also create a generic Admin user. You will get an error if Docker is not running.
1. Run `make airflow` to spin up Airflow and attach a shell to one of the containers
1. Open a web browser and navigate to `localhost:8080` to see your own local webserver. A generic Admin user is automatically created for you in MR airflow instances with the username and password set to `admin`.
1. In the airflow shell, run a command to trigger the DAG/Task you want to test, for example `airflow run snowflake_load snowflake-load 2019-01-01` (as configured in the docker-compose file, all kube pods will be created in the `testing` namespace). Or if you want to run an entire DAG (for instance the `dbt` DAG to test the branching logic), the command would be something like `airflow backfill dbt -s 2019-01-01T00:00:00 -e 2019-01-01T00:00:00`.
1. Once the job is finished, you can navigate to the DAG/Task instance to review the logs.

There is also a `make help` command that describes what commands exist and what they do.

Some gotchas:

* Ensure you have the latest version of Docker. This will prevent errors like `ERROR: Version in “./docker-compose.yml” is unsupported.`
* If you're calling a new python script in your dag, ensure the file is executable by running `chmod +x your_python_file.py`. This will avoid permission denied errors.
* Ensure that any new secrets added in your dag are also in `kube_secrets.py`. This is the source of truth for which secrets Airflow uses. The actual secret value isn't stored in this file, just the pointers.
* If your images are outdated, use the command `docker pull <image_name>` to force a fresh pull of the latest images.

### Python Housekeeping

There are multiple `make` commands and CI jobs designed to help keep the repo's python clean and maintainable. The following commands in the Makefile will help analyze the repo:

* `make lint` will run the `black` python linter and update files (this is not just a check)
* `make pylint` will run the pylint checker but will NOT check for code formatting, as we use `black` for this. This will check for duplicated code, possible errors, warnings, etc. General things to increase code quality. It ignores the DAGs dir as those are not expected to follow general code standards.
* `make radon` will test relevant python code for cyclomatic complexity and show functions or modules with a score of `B` or lower.
* `make xenon` will run a complexity check that returns a non-zero exit code if the threshold isn't met. It ignores the `shared_modules` and `transform` repos until they get deleted/deprecated or updated at a later date.

### Video Walk Throughs

* [Airflow pt 1](https://drive.google.com/open?id=1S03mMINXJFXekeixcJS2tN4T62qYchej)
* [Airflow pt 2](https://drive.google.com/open?id=1zZGtSZIvSwHvhu2sEgGm4LjvbLim5KME)

#### Project variables

Our current implementation uses the following project variables:

  - SNOWFLAKE_ACCOUNT
  - SNOWFLAKE_REPORT_WAREHOUSE
  - SNOWFLAKE_{FLAVOR}_USER
  - SNOWFLAKE_{FLAVOR}_PASSWORD
  - SNOWFLAKE_{FLAVOR}_DATABASE
  - SNOWFLAKE_{FLAVOR}_ROLE
  - SNOWFLAKE_{FLAVOR}_WAREHOUSE

The following flavors are defined:

  - `LOAD` flavor is used by the Extract & Load process
  - `TRANSFORM` flavor is used by the Transform process
  - `TEST` flavor for testing using Snowflake
  - `PERMISSION` flavor for the permission bot
  - `SYSADMIN` flavor for housekeeping tasks (like setting up review instances). This flavor doesn't define `SNOWFLAKE_SYSADMIN_DATABASE` and `SNOWFLAKE_SYSADMIN_WAREHOUSE`.


The following variables are set at the job level dependending on the running environment **and should not set in the project settings**.
{: .panel-heading}
  - SNOWFLAKE_USER
  - SNOWFLAKE_PASSWORD
  - SNOWFLAKE_ROLE
  - SNOWFLAKE_DATABASE
  - SNOWFLAKE_WAREHOUSE

## Accessing peered VPCs

Some of the GitLab specific ELTs connect to databases which are in peered GCP projects, such as the usage ping. To allow connections, a few actions have been taken:
1. The Kubernetes cluster where the runner executes has been setup to use [IP aliasing](https://cloud.google.com/kubernetes-engine/docs/how-to/ip-aliases), so each pod gets a real routable IP within GCP.
1. A [VPC peering relationship](https://cloud.google.com/vpc/docs/vpc-peering) has been established between the two projects and their networks.
1. A firewall rule has been created in the upstream project to allow access from the runner Kubernetes cluster's pod subnet.

### Hosts Records Dataflow

From our on-premises installations, we recieve [version and ping information](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html) from the software. This data is currently imported once a day from a PostgreSQL database into our enterprise data warehouse (EDW). We use this data to feed into Salesforce (SFDC) to aid our sales representatives in their work.

The domains from all of the pings are first cleaned by standardizing the URL using a package called [tldextract](https://github.com/john-kurkowski/tldextract). Each cleaned ping type is combined into a single host record. We make a best effort attempt to align the pings from the same install of the software.

This single host record is then enriched with data from three sources: DiscoverOrg, Clearbit, and WHOIS. If DiscoverOrg has no record of the domain we then fallback to Clearbit, with WHOIS being a last resort. Each request to DiscoverOrg and Clearbit is cached in the database and is updated no more than every 30 days. The cleaning and enrichment steps are all accomplished using Python.

We then take all of the cleaned records and use dbt to make multiple transformations. The last 60 days of pings are aligned with Salesforce accounts using the account name or the account website. Based on this, tables are generated of host records to upload to SFDC. If no accounts are found, we then generate a table of accounts to create within SFDC.

Finally, we use Python to generate SFDC accounts and to upload the host records to the appropriate SFDC account. We also generate any accounts necessary and update any SFDC accounts with DiscoverOrg, Clearbit, and WHOIS data if any of the relevant fields are not already present in SFDC.

## Extractor Documentation

* [BambooHR](https://gitlab.com/gitlab-data/analytics/tree/master/extract/bamboohr)
* [Commit Stats (Engineering)](https://gitlab.com/gitlab-data/analytics/tree/master/extract/engineering)
* [YML files from the handbook](https://gitlab.com/gitlab-data/analytics/tree/master/extract/gitlab_data_yaml)
* [SheetLoad](https://gitlab.com/gitlab-data/analytics/tree/master/extract/sheetload)


