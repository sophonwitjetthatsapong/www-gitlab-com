---
layout: markdown_page
title: "Bar Raiser Interview"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Bar Raiser Interview

For every Manager-level hire and above, a Bar Raiser Interview will be conducted by a Team Member who does not work in the same department as the vacancy. Though, some departments may choose to conduct Bar Raiser Interviews for Individual Contributor vacancies.

The Bar Raiser Interviewer is:
   * At least one level senior to the vacancy.
   * Has completed Bar Raiser training.

The Bar Raiser Interviewer:   

  * Interviews a candidate towards the end of the interview process.
  * Are looking for general intellectual capacity and problem solving capabilities, often through case-style interviews.
     * For example, in the UX department, a Product Manager conducts the final interview for Individual Contributor candidates and a Product Director does the same for Manager-level candidates.
  * Assesses candidates based on a [values](/handbook/values/) fit and long-term success at GitLab.
  * May overrule the Hiring Manager's recommendation to move forward with a candidate.     

The Bar-Raiser program is influenced by Amazon, who [created this practice](https://blog.aboutamazon.com/working-at-amazon/how-amazon-hires) in order to hire well, rather than to hire quickly.
