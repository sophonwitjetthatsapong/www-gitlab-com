---
layout: job_family_page
title: "Director, Global People Operations"
---

The Director of Global People Operations is responsible for overseeing the employee experience throughout the employee lifecycle. This position will report to the Chief People Officer and work closely with the organizational leaders in shaping the Gitlab cultural strategies to support ongoing innovation and change. This position will use their deep experience in HR areas of labor relations, compensation, benefits, organizational strategy, employee lifecycle activities, and compliance with HR policies and procedures to enable Gitlab's rapid growth.

## Responsibilities

* Lead and manage a growing People Operations team responsible for managing the employee experience throughout the employee lifecycle.
* Help shape a global People Operations strategy that aligns and moves the business towards continued growth, innovation and improvement.
* Provide HR support and consultation to the business; answering employee and manager questions about HR programs, policies, and other HR-related items.
* Provide HR support and leadership with respect to day-to-day HR life cycle activities of client groups, including performance management issues, investigations, and reorganizations.
* Understand workforce needs as the company scales managing costs while staying competitive with salary, benefits, perks, leaves, etc.
* Work with the executive team to come up with a competitive compensation strategy.
* Assess and apply market data, ensuring alignment with the company’s total compensation philosophy.
* Oversee performance, development, and the compensation review process.
* Drive a progressive, proactive, positive culture, and increase levels of engagement, enablement, and retention.
* Coach, influence, and provide guidance to business partners to figure out optimal organizational design, development, and performance management plans.
* Assist with the maintenance and accuracy of HR data, including processing SAP data, such as Employee Action Forms.
* Provide resolution to employee and organizational issues in a proactive and sensitive manner.
* Assess/identify HR strategy, policy, or process improvements.
* Ensure People Operations strategies and processes remain aligned with the company’s talent management and workforce plans to enhance employee engagement and sustain business growth.
* Collaborate with Chief People Officer, People Operations, and organizational leaders in fostering a culture of empowerment and performance; establish the employee lifecycle journey and succession plans.
* Maintain in-depth knowledge of local, global, and federal employment laws; maintain and store records judiciously and securely.


## Requirements

* 8+ years of progressive experience in HR roles with a demonstrable track record of building and optimizing processes, systems, and structures.
* Requires a Bachelor’s degree preferably in Human Resources, Organizational Development, Organizational Leadership, or related field.
* 5+ years hands-on experience with compensation & benefits.
* Labor Relations experience required, preferably in multiple countries.
* Working knowledge of regulatory and legal requirements related to total rewards and systems.
* 3-5 years of experience leading people and cross-functional organizations.
* Demonstrable ability to own, execute and deliver on short- and long-term projects.
* Strategic and innovative thinker; able to prioritize and use sound judgment and decision-making.
* Executive presence with excellent written and oral communication skills.
* Strong business insight and high EQ to successfully collaborate with executives and business partners at all levels.
* Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
