---
layout: job_family_page
title: "General Ledger Accountant"
---

GitLab is adding the next essential member who can help support the accounting team by ensuring accurate ontime results in compliance with GAAP

## Responsibilities

* Ensure an accurate and timely month end close by preparing various journal entries and account reconciliations for cash, prepaids, accruals, inter-company, fixed assets and various other accounts
* Coordinate with other departments to obtain transaction information, research reconciling items, and resolve issues
* Assist with daily banking requirements as needed
* Ensure proper accounting policies and principles are followed in accordance with GAAP
* Assist in project implementation of new procedures to enhance the workflow of the department
* Ensure compliance of internal controls as related to SOX
* Assist in quarterly reviews and annual audit with external auditors
* Support overall department goals and objectives
* Provide backup support to the accounts payable team as needed
* Assist with ad-hoc projects and management inquiries
 
## Requirements

* Strong working knowledge of GAAP principles and financial statements
* 5+ years of related accounting experience, public company accounting experience is a plus
* Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
* Must have experience with Netsuite
* Proficient with excel and google sheets
* International experience preferred.
* Detail-oriented, self-directed and able to effectively complete tasks with minimal supervision
* Proficiency with GitLab
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-group/code-of-conduct/#background-checks).

## Performance Indicators

- [Average days to close](/handbook/finance/accounting/#average-days-to-close-kpi-definition)
- [Number of material audit adjustments = 0](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of ineffective Sox Controls = 0%](/handbook/internal-audit/#internal-audit-performance-measures)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
