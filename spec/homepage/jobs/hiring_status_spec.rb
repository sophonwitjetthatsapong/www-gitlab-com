# frozen_string_literal: true

describe Gitlab::Homepage::Jobs::HiringStatus do
  describe '#under_sanction?' do
    it 'return true when sanction: true' do
      subject = described_class.new({ 'sanction' => true })

      expect(subject.under_sanction?).to be_truthy
    end

    it 'return false when sanction: false' do
      subject = described_class.new({ 'sanction' => false })

      expect(subject.under_sanction?).to be_falsey
    end
  end

  describe '#hiring?' do
    it 'return true when hiring: true' do
      subject = described_class.new({ 'hiring' => true })

      expect(subject.hiring?).to be_truthy
    end

    it 'return false when hiring: false' do
      subject = described_class.new({ 'hiring' => false })

      expect(subject.hiring?).to be_falsey
    end
  end

  describe '#hiring_limit' do
    context 'when not under sanction' do
      it 'return defined hiring limit when one is defined' do
        subject = described_class.new({ 'hiring_limit' => 10 })

        expect(subject.hiring_limit).to eq(10)
      end

      it 'return "no limit" when set to "no" or "false"' do
        subject = described_class.new({ 'hiring_limit' => false })

        expect(subject.hiring_limit).to eq('no limit')
      end
    end

    context 'when under sanction' do
      it 'return N/A' do
        subject = described_class.new({ 'hiring_limit' => 10, 'sanction' => true })

        expect(subject.hiring_limit).to eq('N/A')
      end
    end
  end
end
